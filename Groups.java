package com.vivek.splitwise;

import lombok.Data;
import lombok.ToString;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * @author vivek.garg on 24/12/18
 */
@Data
@ToString
public class Groups {

    private Long id;
    private String name;
    private List<Long> userIds;

    public Groups(String name) {
        this.id = Constants.getGroupId();
        this.name = name;
        userBalanceMap = new HashMap<>();
    }

    public Groups(String name, List<Long> userIds) {
        this.id = Constants.getGroupId();
        this.name = name;
        this.userIds = userIds;
        userBalanceMap = new HashMap<>();
    }

    public void addUser(long userId){
        this.userIds.add(userId);
    }


    private HashMap<Long, List<BillAmount>> userBalanceMap;

}
