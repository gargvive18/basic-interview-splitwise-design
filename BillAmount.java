package com.vivek.splitwise;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * @author vivek.garg on 24/12/18
 */
@Data
@ToString
@AllArgsConstructor
public class BillAmount {

    private int userId;

    private double amount;




}
