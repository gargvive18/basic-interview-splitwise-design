package com.vivek.splitwise;

import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vivek.garg on 24/12/18
 */
@Data
@ToString
public class Users {

    private int id;
    private String name;
    private List<Long> groupIds;

    // Overall balance
    private double currentBalance;

    public Users(String name) {
        this.id = (int) Constants.getUserId();
        this.name = name;
        this.currentBalance = 0;
        groupIds = new ArrayList<>();
    }

    public Users(String name, List<Long> groupIds) {
        this.id = (int) Constants.getGroupId();
        this.name = name;
        this.groupIds = groupIds;
        this.currentBalance = 0;
    }

    public void addInGroup(long groupId){
        this.groupIds.add(groupId);
    }

    public void updateCurrentBalance(double amount){
        this.currentBalance+=amount;
    }

}
