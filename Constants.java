package com.vivek.splitwise;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author vivek.garg on 24/12/18
 */
public class Constants {
    private static final AtomicLong counterGroupId = new AtomicLong(0);

    public static long getGroupId(){
        return counterGroupId.incrementAndGet();
    }

    private static final AtomicLong counterUserId = new AtomicLong(0);

    public static long getUserId(){
        return counterUserId.incrementAndGet();
    }
}
