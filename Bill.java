package com.vivek.splitwise;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author vivek.garg on 24/12/18
 */
@Data
@ToString
@AllArgsConstructor
public class Bill {

    private int id;
    private long groupId;
//    private List<BillAmount> billAmountBreakUp;
    private List<Long> usersInvolved;

    private Long paidByUserId;
    private Double paidAmount;

}
