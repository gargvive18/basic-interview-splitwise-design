package com.vivek.splitwise;

import javafx.util.Pair;

import java.util.*;

/**
 * @author vivek.garg on 24/12/18
 */
public class SplitwiseMainApp {

    static List<Users> usersList = new ArrayList<>();
    static List<Groups> groupsList = new ArrayList<>();

    public static void main(String[] args) throws Exception {

        Users user1 = new Users(UUID.randomUUID().toString());
        Users user2 = new Users(UUID.randomUUID().toString());
        Users user3 = new Users(UUID.randomUUID().toString());
        Users user4 = new Users(UUID.randomUUID().toString());
        Users user5 = new Users(UUID.randomUUID().toString());
        usersList.add(user1);
        usersList.add(user2);
        usersList.add(user3);
        usersList.add(user4);
        usersList.add(user5);

        List<Long> group1Users = new ArrayList<>();
//        group1Users.add((long) user1.getId());
//        group1Users.add((long) user2.getId());
//        group1Users.add((long) user3.getId());

        Groups group1 = new Groups("Group1", group1Users);
        groupsList.add(group1);

        addUser((long) user1.getId(),group1.getId());
        addUser((long) user2.getId(),group1.getId());
        addUser((long) user3.getId(),group1.getId());
        addUser((long) user4.getId(),group1.getId());

        printUserList();
        printGroupList();

        Bill bill1 = new Bill(1,1l,group1Users,1l, 100.0);
        addBill(bill1);

        printUserList();
        printGroupList();

    }

    private static void printGroupList() {

        System.out.println("************** GROUP LIST ********************");
        groupsList.forEach(groups -> System.out.println(groups));
    }

    private static void printUserList() {

        System.out.println("************** USER LIST ********************");
        usersList.forEach(users -> System.out.println(users));
    }

    public static void addUser(Long userId, Long groupId) throws Exception {

        // Update group
        Pair<Groups,Integer> groupPair = getGroup(groupId);
        Groups groups = groupPair.getKey();
        groups.addUser(userId);
        groupsList.set(Math.toIntExact(groupPair.getValue()), groups);

        // update user
        Pair<Users,Integer> userPair = getUsers(userId);
        Users users = userPair.getKey();
        users.addInGroup(groupId);
        usersList.set(Math.toIntExact(userPair.getValue()), users);

    }


    public static void addBill(Bill bill) throws Exception {

        Pair<Groups,Integer> groupPair = getGroup(bill.getGroupId());
        Groups groups = groupPair.getKey();

        Pair<Users,Integer> userPair = getUsers(bill.getPaidByUserId());
        Users paidByUser = userPair.getKey();

        double amountToSplit = bill.getPaidAmount();

        List<Long> userInvolved = bill.getUsersInvolved();
        int noOfUsersInvolvedInBill = userInvolved.size();

        double splittedAmount = amountToSplit/(1.0*noOfUsersInvolvedInBill);

        paidByUser.updateCurrentBalance(amountToSplit);
        usersList.set(Math.toIntExact(userPair.getValue()), paidByUser);

        for(int i=0;i<userInvolved.size(); i++){

            // Update user current balance
            Pair<Users,Integer> usePair = getUsers(userInvolved.get(i));
            Users user = usePair.getKey();
            user.updateCurrentBalance(-1*splittedAmount);
            usersList.set(Math.toIntExact(usePair.getValue()), user);

            // update userbalancemap

            HashMap<Long,List<BillAmount>> userBalanceMap = groups.getUserBalanceMap();

            List<BillAmount> list;
            if(userBalanceMap.get(user.getId())!=null){
                list = userBalanceMap.get(user.getId());
                list.add(new BillAmount(paidByUser.getId(), -1*splittedAmount));
            }else{
                list = new ArrayList<>();
                list.add(new BillAmount(paidByUser.getId(), -1*splittedAmount));
            }
            userBalanceMap.put((long) user.getId(),list);
            groups.setUserBalanceMap(userBalanceMap);
        }

        groupsList.set(Math.toIntExact(groupPair.getValue()),groups);

    }


    private static Pair<Groups,Integer> getGroup(long groupId) throws Exception {
        int totalGroups = groupsList.size();

        for(int i=0; i< totalGroups ; i++){
            if(groupsList.get(i).getId() == groupId){
                return new Pair(groupsList.get(i), i);
            }
        }

        throw new Exception("GroupId not found");
    }


    private static Pair<Users,Integer> getUsers(long userId) throws Exception {
        int totalUsers = usersList.size();

        for(int i=0; i< totalUsers ; i++){
            if(usersList.get(i).getId() == userId){
                return new Pair(usersList.get(i), i);
            }
        }
        throw new Exception("UserId not found");
    }
}
